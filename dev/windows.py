import tkinter as tk
from tkinter import filedialog
# audio playback
import pygame
# text to speech
from gtts import gTTS
# image text extraction
import pytesseract
from PIL import Image
# temp file
import tempfile
import os

pygame.init()

# Initialize pygame mixer
pygame.mixer.init()

# Variables to hold the loaded audio and play/pause status
current_audio = None
is_playing = False
just_loaded = False

def load_audio():
    global current_audio
    global just_loaded
    file_path = filedialog.askopenfilename()
    current_audio = pygame.mixer.Sound(file_path)
    just_loaded = True
    print("Audio loaded:", file_path)

def check_file_type(file_path):
    image_extensions = ['.jpg', '.jpeg', '.png', '.gif', '.bmp']
    audio_extensions = ['.mp3', '.wav', '.ogg', '.flac', '.aac']

    file_extension = file_path[file_path.rfind('.'):]
    file_extension = file_extension.lower()

    if file_extension in image_extensions:
        return 'image'
    elif file_extension in audio_extensions:
        return 'audio'
    else:
        return None

# Generate the audio
def on_generate_button_clicked(self, widget):
    filePath = self.file_chooser_button.get_filename()
    try:
        if self.image_select:
            print(self.image_select)
            img = Image.open(filePath)
            text = pytesseract.image_to_string(img)
            
            # Convert text to speech
            self.temp_file = tempfile.NamedTemporaryFile(suffix=".mp3", delete=False)
            tts = gTTS(text=text, lang='en')
            tts.save(self.temp_file.name)
            self.temp_file.close()
            self.timestamp_label.set_text("0:00/0:00")
        else:
            print(self.image_select)
            # Read the text from the file
            with open(filePath, 'r') as file:
                text = file.read()

            # Convert text to speech
            self.temp_file = tempfile.NamedTemporaryFile(suffix=".mp3", delete=False)
            tts = gTTS(text=text, lang='en')
            tts.save(self.temp_file.name)
            self.temp_file.close()
            self.timestamp_label.set_text("0:00/0:00")
        dialog = Gtk.MessageDialog(
            transient_for=self,
            flags=0,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text="Finished Generating",
        )
        dialog.format_secondary_text(
            "Your audio is ready."
        )
        dialog.run()
        print("INFO dialog closed")

        dialog.destroy()
    except:
        dialog = Gtk.MessageDialog(
            transient_for=self,
            flags=0,
            message_type=Gtk.MessageType.ERROR,
            buttons=Gtk.ButtonsType.CANCEL,
            text="Error, Invalid File Selected",
        )
        dialog.format_secondary_text(
            "Please select either a valid file using the file selector, or change the selected file type."
        )
        dialog.run()

        print("ERROR dialog closed")
        dialog.destroy()


def play_pause_audio():
    global is_playing
    global current_audio
    global just_loaded
    
    if current_audio:
        if not is_playing:
            if just_loaded:
                current_audio.play()
                is_playing = True
                just_loaded = False
                print("Audio started playing")
            else:
                pygame.mixer.unpause()
                is_playing = True
                print("Audio resumed")
        else:
            pygame.mixer.pause()
            is_playing = False
            print("Audio paused")

def stop_audio():
    global is_playing
    global current_audio
    
    if current_audio:
        pygame.mixer.stop()
        current_audio = None
        is_playing = False
        print("Audio stopped")

root = tk.Tk()
root.title("Audio Player")

padx = 12
pady = 12

button1 = tk.Button(root, text="Load Audio", command=load_audio)
button1.grid(row=0, column=0, columnspan=2, padx=padx, pady=pady, sticky="ew")

button2 = tk.Button(root, text="Play/Pause", command=play_pause_audio)
button2.grid(row=1, column=0, padx=padx, pady=pady, sticky="ew")

button3 = tk.Button(root, text="Stop", command=stop_audio)
button3.grid(row=1, column=1, padx=padx, pady=pady, sticky="ew")

root.mainloop()

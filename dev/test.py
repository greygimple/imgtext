import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio

class CenteredTextWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Centered Text Window")
        self.set_default_size(300, 200)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        label = Gtk.Label(label="Centered Text")
        label.set_hexpand(True)
        label.set_vexpand(True)
        box.pack_start(label, True, True, 0)

        self.add(box)
        self.set_position(Gtk.WindowPosition.CENTER)

class AboutWindow(Gtk.AboutDialog):
    def __init__(self, parent):
        super().__init__()
        self.set_program_name("MyApp")
        self.set_comments("This is an about window example.")
        self.set_version("1.0")
        self.set_authors(["Author Name"])
        self.set_website("https://www.example.com")
        self.set_license_type(Gtk.License.GPL_3_0)
        self.set_wrap_license(True)
        self.set_transient_for(parent)
        self.connect("response", lambda dialog, _: dialog.destroy())

        # Set initial portrait size
        self.set_default_size(300, 400)  # Width, Height
        
        # Set the application icon in the AboutDialog
        icon_theme = Gtk.IconTheme.get_default()
        icon = icon_theme.load_icon("applications-system", 48, 0)  # Icon name and size
        self.set_logo(icon)  # Set the icon as the logo for AboutDialog

class HeaderBarWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="HeaderBar Demo")
        self.set_border_width(10)
        self.set_default_size(400, 200)

        # Set the application icon
        icon_theme = Gtk.IconTheme.get_default()
        icon = icon_theme.load_icon("applications-system", 48, 0)  # Icon name and size
        self.set_icon(icon)

        # Headerbar
        self.headerbar = Gtk.HeaderBar()
        self.headerbar.set_show_close_button(True)
        self.headerbar.props.title = "Image and Text File Selector"
        self.set_titlebar(self.headerbar)

        # Hamburger Menu
        # Drop Down Menu
        self.hamburger_popover = Gtk.Popover()
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vbox.set_margin_start(6)
        vbox.set_margin_end(6)
        item1_button = Gtk.ModelButton(label="Item 1")
        item1_button.connect("clicked", self.on_item1_clicked)
        item2_button = Gtk.ModelButton(label="Item 2")
        item2_button.connect("clicked", self.on_item2_clicked)
        vbox.pack_start(item1_button, False, True, 10)
        vbox.pack_start(item2_button, False, True, 10)
        vbox.show_all()
        self.hamburger_popover.add(vbox)
        self.hamburger_popover.set_position(Gtk.PositionType.BOTTOM)

        # HeaderBar Button
        self.hamburger_button = Gtk.MenuButton(popover=self.hamburger_popover)
        icon = Gio.ThemedIcon(name="open-menu-symbolic")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.hamburger_button.add(image)
        self.headerbar.pack_end(self.hamburger_button)

    def on_item1_clicked(self, widget):
        centered_win = CenteredTextWindow()
        centered_win.connect("destroy", self.on_centered_text_window_closed)
        centered_win.show_all()

    def on_item2_clicked(self, widget):
        about_win = AboutWindow(self)
        about_win.show_all()

    def on_centered_text_window_closed(self, widget):
        # Handle the event when the CenteredTextWindow is closed
        pass

win = HeaderBarWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()

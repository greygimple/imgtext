#!/bin/bash

# Function to install Python packages from requirements.txt
install_packages() {
    pip3 install -r requirements.txt
}

# Function to install the script and .desktop file for user-level installation
install_user() {
    # Define the installation directory for user-level installation
    INSTALL_DIR="$HOME/.local/bin"
    
    # Copy the Python script to the installation directory
    cp imgtext.py "$INSTALL_DIR/imgtext.py"

    # Install required Python packages
    install_packages

    # Ensure the script is executable
    chmod +x "$INSTALL_DIR/imgtext.py"
}

# Function to install the script and .desktop file for system-level installation
install_system() {
    # Define the installation directory for system-level installation
    INSTALL_DIR="/usr/local/bin"
    
    # Copy the Python script to the installation directory
    sudo cp imgtext.py "$INSTALL_DIR/imgtext.py"

    # Install required Python packages
    install_packages

    # Ensure the script is executable
    sudo chmod +x "$INSTALL_DIR/imgtext.py"  
}

# Function to uninstall the script for user or system level installation
uninstall() {
    if [ -e "$HOME/.local/bin/imgtext.py" ]; then
        rm "$HOME/.local/bin/imgtext.py"
    elif [ -e "/usr/local/bin/imgtext.py" ]; then
        sudo rm "/usr/local/bin/imgtext.py"
    else
        echo "Nothing to uninstall."
    fi
}

# Function to uninstall the script for user or system level installation and removes installed dependencies
purge() {
    if [ -e "$HOME/.local/bin/imgtext.py" ]; then
        rm "$HOME/.local/bin/imgtext.py"
    elif [ -e "/usr/local/bin/imgtext.py" ]; then
        sudo rm "/usr/local/bin/imgtext.py"
    else
        echo "Nothing to uninstall."
    fi
    # Remove the installed Python packages
    sudo pip3 uninstall -r requirements.txt
}

# Check the first argument for install/uninstall and the second argument for user/system
if [ "$1" == "install" ]; then
    if [ "$2" == "user" ]; then
        install_user
    elif [ "$2" == "system" ]; then
        install_system
    else
        echo "Invalid command. Usage: $0 install user/system"
        exit 1
    fi
elif [ "$1" == "uninstall" ]; then
    uninstall
elif [ "$1" == "purge" ]; then
    purge
else
    echo "Invalid command. Usage: $0 install user/system, uninstall, purge"
    exit 1
fi

echo "All operations completed."

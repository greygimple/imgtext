# text to speech
from gtts import gTTS
# gtk3
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gst, GLib, Gio
# image text extraction
import pytesseract
from PIL import Image
# temp file
import tempfile
import os

class HelpWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Help Window")
        self.set_default_size(300, 200)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        label = Gtk.Label(label="Coming Soon")
        label.set_hexpand(True)
        label.set_vexpand(True)
        box.pack_start(label, True, True, 0)

        self.add(box)
        self.set_position(Gtk.WindowPosition.CENTER)

class AboutWindow(Gtk.AboutDialog):
    def __init__(self, parent):
        super().__init__()
        self.set_program_name("ImgText")
        self.set_comments("Reads text from images and text files.")
        self.set_version("1.0")
        self.set_authors(["Grey Gimple"])
        self.set_license_type(Gtk.License.GPL_3_0)
        self.set_wrap_license(True)
        self.set_copyright("\xa9 2023 The ImgText author(s)")
        self.set_website("https://gitlab.com/greygimple/imgtext")
        self.set_transient_for(parent)
        self.connect("response", lambda dialog, _: dialog.destroy())

        # Set the application icon in the AboutDialog
        icon_theme = Gtk.IconTheme.get_default()
        icon = icon_theme.load_icon("applications-system", 48, 0)  # Icon name and size
        self.set_logo(icon)  # Set the icon as the logo for AboutDialog

class ImageAndTextReader(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Image and Text File Selector")
        self.set_default_size(600, 300)
        self.set_default_icon_name("search-symbolic")
        self.connect("delete-event", self.on_delete_event)

        # Set the application icon
        icon_theme = Gtk.IconTheme.get_default()
        icon = icon_theme.load_icon("applications-system", 48, 0)  # Icon name and size
        self.set_icon(icon)

        self.is_playing = False
        self.temp_file = None
        self.image_select = True

        # Initialize GStreamer
        Gst.init(None)

        # Create GStreamer pipeline
        self.pipeline = Gst.Pipeline.new("audio-player")

        # Create the necessary elements for audio playback
        self.playbin = Gst.ElementFactory.make("playbin", "playbin")
        self.pipeline.add(self.playbin)

        # Headerbar
        self.headerbar = Gtk.HeaderBar()
        self.headerbar.set_show_close_button(True)
        self.headerbar.props.title = "ImgText"
        self.set_titlebar(self.headerbar)

        # Hamburger Menu
        # Drop Down Menu
        self.hamburger_popover = Gtk.Popover()
        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.vbox.set_margin_top(6)
        self.vbox.set_margin_bottom(6)
        self.vbox.set_margin_start(6)
        self.vbox.set_margin_end(6)
        self.help_button = Gtk.ModelButton(label="Help")
        self.help_button.connect("clicked", self.on_help_clicked)
        self.about_button = Gtk.ModelButton(label="About ImgText")
        self.about_button.connect("clicked", self.on_about_clicked)
        self.vbox.pack_start(self.help_button, False, True, 0)
        self.vbox.pack_start(self.about_button, False, True, 0)
        self.vbox.show_all()
        self.hamburger_popover.add(self.vbox)
        self.hamburger_popover.set_position(Gtk.PositionType.BOTTOM)

        # HeaderBar Button
        self.hamburger_button = Gtk.MenuButton(popover=self.hamburger_popover)
        icon = Gio.ThemedIcon(name="open-menu-symbolic")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.hamburger_button.add(image)
        self.headerbar.pack_end(self.hamburger_button)

        # Create a vertical box to hold elements
        self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(self.main_box)

        # Create a horizontal box to hold elements
        self.top_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.main_box.pack_start(self.top_box, True, True, 0)

        # Box for radio buttons (on the left)
        self.left_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.left_box.set_margin_bottom(0)
        self.left_box.set_margin_top(12)
        self.left_box.set_margin_start(12)
        # self.left_box.set_margin_end(12)
        self.top_box.pack_start(self.left_box, False, False, 0)

        # Box to center the radio buttons vertically and horizontally
        self.center_left_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.left_box.pack_start(self.center_left_box, True, True, 0)

        # Radio buttons for Image File and Text File
        self.image_radio = Gtk.RadioButton.new_with_label_from_widget(None, "Image File")
        self.text_radio = Gtk.RadioButton.new_with_label_from_widget(self.image_radio, "Text File")
        self.text_radio.connect("toggled", self.on_radio_toggled)

        # Add radio buttons to the centered box
        self.center_left_box.pack_start(self.image_radio, True, True, 0)
        self.center_left_box.pack_start(self.text_radio, True, True, 0)

        # Box for buttons and file chooser (on the right)
        self.right_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.right_box.set_hexpand(True)
        self.right_box.set_margin_bottom(0)
        self.right_box.set_margin_top(12)
        self.right_box.set_margin_start(12)
        self.right_box.set_margin_end(12)
        self.top_box.pack_start(self.right_box, True, True, 0)

        # Button to generate audio
        # self.generate_button = Gtk.Button(label="Generate Audio")
        self.generate_button = Gtk.Button.new_from_icon_name("preferences-system-symbolic", Gtk.IconSize.LARGE_TOOLBAR)
        self.generate_button.set_tooltip_text("Generate Audio")
        self.generate_button.connect("released", self.on_generate_button_clicked)
        self.generate_button.set_hexpand(True)
        self.right_box.pack_start(self.generate_button, True, True, 0)

        # Box for buttons (on the right)
        self.button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.button_box.set_hexpand(True)
        self.right_box.pack_start(self.button_box, True, True, 0)

        # Button to play/pause audio
        self.play_pause_button = Gtk.Button()
        self.play_pause_image = Gtk.Image.new_from_icon_name("media-playback-start", Gtk.IconSize.LARGE_TOOLBAR)
        self.play_pause_button.set_image(self.play_pause_image)
        self.play_pause_button.set_tooltip_text("Play/Pause Playback")
        self.play_pause_button.connect("clicked", self.on_play_pause_button_clicked)
        self.play_pause_button.set_hexpand(True)
        self.button_box.pack_start(self.play_pause_button, True, True, 0)

        # Button to stop audio
        self.stop_button = Gtk.Button.new_from_icon_name("media-playback-stop", Gtk.IconSize.LARGE_TOOLBAR)
        self.stop_button.set_tooltip_text("Stop Playback")
        self.stop_button.connect("clicked", self.on_stop_button_clicked)
        self.stop_button.set_hexpand(True)
        self.button_box.pack_start(self.stop_button, True, True, 0)

        # Box for slider
        self.slider_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.slider_box.set_margin_bottom(0)
        self.slider_box.set_margin_top(0)
        self.slider_box.set_margin_start(12)
        self.slider_box.set_margin_end(12)
        self.main_box.pack_start(self.slider_box, False, False, 0)
        
        # Labels for time information
        self.timestamp_label = Gtk.Label(label="-:--/-:--")
        self.left_box.pack_start(self.timestamp_label, False, False, 0)

        # Create slider for seeking
        self.slider = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL, 0, 100, 1)
        self.slider.set_draw_value(False)
        self.slider.set_hexpand(True)
        self.slider.connect("value-changed", self.on_slider_change)
        self.slider.set_sensitive(False)
        self.slider_box.pack_start(self.slider, True, True, 0)

        # Box for file chooser dialog
        self.dialog_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.main_box.pack_start(self.dialog_box, False, False, 0)

        # Filter for any image file
        self.image_filter = Gtk.FileFilter()
        self.image_filter.set_name("Image Files")
        self.image_filter.add_mime_type("image/*")

        # Filter for any text file
        self.text_filter = Gtk.FileFilter()
        self.text_filter.set_name("Text Files")
        self.text_filter.add_mime_type("text/*")

        # File chooser button with initial image filter
        self.file_chooser_button = Gtk.FileChooserButton(title="Select File")
        self.file_chooser_button.add_filter(self.image_filter)
        self.file_chooser_button.set_margin_top(0)
        self.file_chooser_button.set_margin_bottom(12)
        self.file_chooser_button.set_margin_start(12)
        self.file_chooser_button.set_margin_end(12)
        self.dialog_box.pack_start(self.file_chooser_button, True, True, 0)
    
    # Change the filter for the file selection field
    def on_radio_toggled(self, radio):
        if radio.get_active():
            self.image_select = False
            self.file_chooser_button.remove_filter(self.image_filter)
            self.file_chooser_button.add_filter(self.text_filter)
        else:
            self.image_select = True
            self.file_chooser_button.remove_filter(self.text_filter)
            self.file_chooser_button.add_filter(self.image_filter)
    
    # Generate the audio
    def on_generate_button_clicked(self, widget):
        filePath = self.file_chooser_button.get_filename()
        try:
            if self.image_select:
                print(self.image_select)
                img = Image.open(filePath)
                text = pytesseract.image_to_string(img)
                
                # Convert text to speech
                self.temp_file = tempfile.NamedTemporaryFile(suffix=".mp3", delete=False)
                tts = gTTS(text=text, lang='en')
                tts.save(self.temp_file.name)
                self.temp_file.close()
                self.timestamp_label.set_text("0:00/0:00")
            else:
                print(self.image_select)
                # Read the text from the file
                with open(filePath, 'r') as file:
                    text = file.read()

                # Convert text to speech
                self.temp_file = tempfile.NamedTemporaryFile(suffix=".mp3", delete=False)
                tts = gTTS(text=text, lang='en')
                tts.save(self.temp_file.name)
                self.temp_file.close()
                self.timestamp_label.set_text("0:00/0:00")
            dialog = Gtk.MessageDialog(
                transient_for=self,
                flags=0,
                message_type=Gtk.MessageType.INFO,
                buttons=Gtk.ButtonsType.OK,
                text="Finished Generating",
            )
            dialog.format_secondary_text(
                "Your audio is ready."
            )
            dialog.run()
            print("INFO dialog closed")

            dialog.destroy()
        except:
            dialog = Gtk.MessageDialog(
                transient_for=self,
                flags=0,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.CANCEL,
                text="Error, Invalid File Selected",
            )
            dialog.format_secondary_text(
                "Please select either a valid file using the file selector, or change the selected file type."
            )
            dialog.run()

            print("ERROR dialog closed")
            dialog.destroy()
    
    def on_play_pause_button_clicked(self, widget):
        if self.temp_file == None:
            dialog = Gtk.MessageDialog(
                transient_for=self,
                flags=0,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.CANCEL,
                text="Error, No Speech Generated",
            )
            dialog.format_secondary_text(
                "Please generate the required audio"
            )
            dialog.run()

            print("ERROR dialog closed")
            dialog.destroy()
        elif self.is_playing:
            self.pipeline.set_state(Gst.State.PAUSED)
            self.play_pause_image.set_from_icon_name("media-playback-start", Gtk.IconSize.LARGE_TOOLBAR)
            self.is_playing = False
        else:
            self.playbin.set_property("uri", "file://" + self.temp_file.name)
            self.pipeline.set_state(Gst.State.PLAYING)
            self.play_pause_image.set_from_icon_name("media-playback-pause", Gtk.IconSize.LARGE_TOOLBAR)
            self.slider.set_sensitive(True)
            self.is_playing = True
            # Start a timer to update the slider and time labels
            GLib.timeout_add(1000, self.update_slider_and_labels)
        
    def on_stop_button_clicked(self, widget):
        self.pipeline.set_state(Gst.State.NULL)
        self.play_pause_image.set_from_icon_name("media-playback-start", Gtk.IconSize.LARGE_TOOLBAR)
        self.is_playing = False
        self.slider.set_value(0)
        self.timestamp_label.set_text("0:00/0:00")
        self.slider.set_sensitive(False)
    
    def on_delete_event(self, widget, event):
        if self.temp_file != None:
            os.unlink(self.temp_file.name)
        Gtk.main_quit()

    def on_slider_change(self, widget):
        # Seek to the specified position when slider value changes
        value = self.slider.get_value()
        duration = self.playbin.query_duration(Gst.Format.TIME)[1]
        seek_time = value * duration / 100
        self.playbin.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, seek_time)
    
    def update_slider_and_labels(self):
        if self.is_playing:
            try:
                _, position = self.playbin.query_position(Gst.Format.TIME)
                _, duration = self.playbin.query_duration(Gst.Format.TIME)

                if duration > 0:
                    new_value = position * 100 / duration
                    if abs(new_value - self.slider.get_value()) >= 1:
                        self.slider.set_value(new_value)
                if position == duration:
                    self.pipeline.set_state(Gst.State.NULL)
                    self.play_pause_image.set_from_icon_name("media-playback-start", Gtk.IconSize.LARGE_TOOLBAR)
                    self.is_playing = False
                    self.slider.set_value(0)
                    self.timestamp_label.set_text("0:00/0:00")
                    self.slider.set_sensitive(False)
                else:
                    self.timestamp_label.set_text(self.format_time(position) + "/" + self.format_time(duration))
            except Exception as e:
                print("Error updating slider and labels:", e)
        return self.is_playing
    
    def on_help_clicked(self, widget):
        centered_win = HelpWindow()
        centered_win.connect("destroy", self.on_centered_text_window_closed)
        centered_win.show_all()

    def on_about_clicked(self, widget):
        about_win = AboutWindow(self)
        about_win.show_all()
    
    def on_centered_text_window_closed(self, widget):
        pass
    
    @staticmethod
    def format_time(time_nanoseconds):
        time_seconds = time_nanoseconds // Gst.SECOND
        minutes = time_seconds // 60
        seconds = time_seconds % 60
        return f"{minutes}:{seconds:02}"

win = ImageAndTextReader()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()






# ImgText

<div align="center">
  <a href="https://gitlab.com/greygimple/imgtext">
    <img src="images/app-screenshot.png?ref_type=heads" alt="App Screenshot">
  </a>
</div>

## About The Project

A GUI Python application to read text in images or text files

## Features

- [x] Image reading

- [x] Text reading

- [x] Audio seeking via sliderbar

- [x] Play/Pause/Stop

## Prerequisites

- Python (>= 3.10.12)

### Python Packages

- PyGObject (>= 3.42.1)

- gTTS (>= 2.4.0)

- Pillow (>= 10.1.0)

- pytesseract (>= 0.3.10)

### Remove Dependencies

> :memo: **Note:** Prerequisite Python packages will be installed during application install

## Download

```bash
git clone https://gitlab.com/greygimple/imgtext.git

cd imgtext
```

## Installation

### User

```bash
./installer.sh install user
```

### System

```bash
./installer.sh install system
```

## Uninstallation

### Basic

```bash
./installer.sh uninstall
```

### Remove Dependencies

> :warning: **Warning:** This has potential to cause damage to the system, as it may remove dependencies other programs require, run at your own risk

```bash
./installer.sh purge
```

## Contact

Want to contribute and help add more features? Fork the project and open a pull request!

If you wish to contact me, you can reach out at greygimple@gmail.com
